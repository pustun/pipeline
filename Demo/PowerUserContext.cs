﻿using Ninject;
using Pipeline;

namespace Demo
{
    public class PowerUserContext : IExecutionContext
    {
        public void AddContextBindings(IKernel kernel)
        {
            kernel.Bind<ApproveDelivery>().To<AproveDeliveryOnBehalfOfCustomer>();
        }

        public void RemoveContextBindings(IKernel kernel)
        {
            kernel.Unbind<ApproveDelivery>();
        }
    }
}