﻿using Pipeline;

namespace Demo
{
    public class ApproveDelivery : BaseStep, IExecutableStep
    {
        public Order Order { get; set; }

        public void Execute()
        {
            ApproveOrderDelivery();
        }

        protected virtual void ApproveOrderDelivery()
        {
            Order.Status = "delivered";

            Logger.Log("Order {0} is delivered", Order.OrderId);
        }
    }

    public class AproveDeliveryOnBehalfOfCustomer : ApproveDelivery
    {
        private readonly IEmailNotifier _emailNotifier;

        public AproveDeliveryOnBehalfOfCustomer(IEmailNotifier emailNotifier)
        {
            _emailNotifier = emailNotifier;
        }

        protected override void ApproveOrderDelivery()
        {
            base.ApproveOrderDelivery();

            _emailNotifier.Send("cutomer@email.com",
                                "You delivery was approved",
                                "Hello we approved that your delivery was successfull. In case you have any questions contact a manager");
        }
    }

    public interface IEmailNotifier
    {
        void Send(string email, string subject, string body);
    }
}