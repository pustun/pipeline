﻿using Ninject;
using Pipeline;

namespace Demo
{
    public class BaseStep
    {
        [Inject]
        public Logger Logger { get; set; }
    }
}