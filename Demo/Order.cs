﻿namespace Demo
{
    public class Order
    {
        public string OrderId { get; set; }

        public string BookName { get; set; }

        public string Status { get; set; }
    }
}