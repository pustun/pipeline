﻿using Pipeline;

namespace Demo
{
    public class ApproveOrder : BaseStep, IExecutableStep
    {
        public Order Order { get; set; }

        public void Execute()
        {
            Logger.Log("Approving order: {0}", Order.OrderId);

            Order.Status = "shipped";
        }
    }
}