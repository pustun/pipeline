﻿using Pipeline;

namespace Demo
{
    public class Login : BaseStep, IExecutableStep
    {
        public UserCredentials _userCredentials;

        public Login(UserCredentials userCredentials)
        {
            _userCredentials = userCredentials;
        }

        public void Execute()
        {
            //fake implementation
            Logger.Log("Login: {0}", _userCredentials.Login);
            
            Logger.Log("Password: {0}", _userCredentials.Password);
        }
    }
}