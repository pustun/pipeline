﻿// ReSharper disable ConvertToLambdaExpression
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Pipeline;
using Rhino.Mocks;

namespace Demo
{
    [TestFixture]
    public class book_store_tests : StepsDrivenBaseTest
    {
        [SetUp]
        public override void Setup()
        {
            base.Setup();

            RegisterDefaultLogger();
        }

        [Test]
        public void customer_should_be_able_to_login_and_order_book()
        {
            //arrange
            var customer = Users.Customer();
            var order = (Order) null;

            //act
            As(customer).Execute(() =>
                {
                    
                    Do<Login>();

                    order = Do<OrderBook, Order>(s => s.Book = "Treasure island");
                });

            //assert
            Assert.That(order, Is.Not.Null);
            Assert.That(order.Status, Is.EqualTo("pending"));
        }

        [Test]
        public void sales_manager_should_be_able_to_change_order_status()
        {
            //arrange
            var manager = Users.Manager();

            var order = new Order
                {
                    BookName = "Treasure island",
                    OrderId = Guid.NewGuid().ToString(),
                    Status = "pending"
                };

            //act
            As(manager).Execute(() =>
                {
                    Do<Login>();

                    Do<ApproveOrder>(s => s.Order = order);
                });

            //assert
            Assert.That(order.Status, Is.EqualTo("shipped"));
        }

        [Test]
        public void customer_approves_delivery()
        {
            //arrange
            var order = new Order
            {
                BookName = "Treasure island",
                OrderId = Guid.NewGuid().ToString(),
                Status = "pending"
            };

            var customer = Users.Customer();

            //act
            As(customer).Execute(() =>
                {
                    Do<Login>();

                    Do<ApproveDelivery>(s => s.Order = order);
                });

            //assert
            Assert.That(order.Status, Is.EqualTo("delivered"));
        }

        [Test]
        public void manager_should_be_able_approve_delivery_on_behalf_of_customer()
        {
            // arrange
            var order = new Order
            {
                BookName = "Treasure island",
                OrderId = Guid.NewGuid().ToString(),
                Status = "pending"
            };

            var emailNotifier = MockRepository.GenerateMock<IEmailNotifier>();

            emailNotifier.Expect(s => s.Send(null, null, null)).IgnoreArguments();

            _kernel.Bind<IEmailNotifier>().ToConstant(emailNotifier);

            var manager = Users.Manager();
            var powerUserContext = new PowerUserContext();

            // act
            Given(powerUserContext).Execute(() =>
            {
                As(manager).Execute(() =>
                {
                    Do<Login>();

                    Do<ApproveDelivery>(s => s.Order = order);
                });

            });

            //assert
            emailNotifier.VerifyAllExpectations();
        }
    }
}
// ReSharper restore ConvertToLambdaExpression