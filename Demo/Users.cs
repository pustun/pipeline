﻿namespace Demo
{
    public class Users
    {
        public static UserAwareContext Customer()
        {
            return new UserAwareContext(new UserCredentials
                {
                    Login = "john",
                    Password = "orange"
                });
        }

        public static UserAwareContext Manager()
        {
            return new UserAwareContext(new UserCredentials
                {
                    Login = "manager",
                    Password = "cherry"
                });
        }
    }
}