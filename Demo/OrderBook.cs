﻿using System;
using Pipeline;

namespace Demo
{
    public class OrderBook : BaseStep, IExecutableStepWithReturnValue<Order>
    {
        public string Book { get; set; }

        public Order Execute()
        {
            Logger.Log("Ordering book: {0}", Book);

            return new Order
                {
                    OrderId = Guid.NewGuid().ToString(),
                    BookName = Book,
                    Status = "pending"
                };
        }
    }
}