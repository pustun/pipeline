﻿using Ninject;
using Pipeline;

namespace Demo
{
    public class UserAwareContext : IExecutionContext
    {
        private readonly UserCredentials _userCredentials;

        public UserAwareContext(UserCredentials userCredentials)
        {
            _userCredentials = userCredentials;
        }

        public void AddContextBindings(IKernel kernel)
        {
            kernel.Bind<UserCredentials>().ToConstant(_userCredentials);
        }

        public void RemoveContextBindings(IKernel kernel)
        {
            kernel.Unbind<UserCredentials>();
        }
    }
}