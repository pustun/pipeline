﻿using Ninject;

namespace Pipeline
{
	public interface IExecutionContext
	{
		void AddContextBindings(IKernel kernel);

		void RemoveContextBindings(IKernel kernel);
	}
}