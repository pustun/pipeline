﻿using System.Collections.Generic;

namespace Pipeline
{
	public class Pipeline
	{
		private IEnumerable<IModule> _modules;

		public Pipeline(IEnumerable<IModule> modules)
		{
			_modules = modules;
		}

		public TResult RunAndReturnValue<TResult>(IExecutableStepWithReturnValue<TResult> step)
		{
			BeforeExecution(step);

			var value = step.Execute();

			AfterExecution(step);

			return value;
		}

		public void Run(IExecutableStep step)
		{
			BeforeExecution(step);

			step.Execute();

			AfterExecution(step);
		}

		private void AfterExecution(IStep step)
		{
			foreach (var module in _modules)
			{
				module.AfterExecution(step);
			}
		}

		private void BeforeExecution(IStep step)
		{
			foreach (var module in _modules)
			{
				module.BeforeExecution(step);
			}
		}
	}
}