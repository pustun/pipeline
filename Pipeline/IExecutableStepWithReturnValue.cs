﻿namespace Pipeline
{
	public interface IExecutableStepWithReturnValue<TReturn> : IStep
	{
		TReturn Execute();
	}
}