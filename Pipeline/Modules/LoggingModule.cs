﻿using System.Linq;
using Pipeline.Attributes;

namespace Pipeline.Modules
{
	public class LoggingModule : IModule
	{
		private readonly ILogger _logger;

		public LoggingModule(ILogger logger)
		{
			_logger = logger;
		}

		public void BeforeExecution(IStep step)
		{
			var logEntry = GetLogEntry(step);

			_logger.Log("# {0} : Started", logEntry);
		}

		public void AfterExecution(IStep step)
		{
			var logEntry = GetLogEntry(step);

			_logger.Log("# {0} : Finished", logEntry);
		}

		private string GetLogEntry(IStep step)
		{
			var logAsAttribute = step.GetType().GetCustomAttributes(typeof(LogAsAttribute), true).FirstOrDefault();

			if (logAsAttribute != null)
			{
				return (logAsAttribute as LogAsAttribute).Text;
			}

			return step.GetType().Name;
		}
	}
}