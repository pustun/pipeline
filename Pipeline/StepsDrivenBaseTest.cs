﻿using System;
using System.Collections.Generic;
using Ninject;
using SeleniumProject.PipelineTests;

namespace Pipeline
{
	public abstract class StepsDrivenBaseTest
	{
	    protected IKernel _kernel;

	    protected Pipeline _pipeline;

		public virtual void Setup()
		{
			_kernel = new StandardKernel();

			_kernel.Bind<IKernel>().ToConstant(_kernel);

			_kernel.Bind<Pipeline>().ToSelf();

			_pipeline = _kernel.Get<Pipeline>();
		}

        protected void RegisterDefaultLogger()
        {
            RegisterLogger<Logger>();
        }

        protected void RegisterLogger<T>() where T : ILogger
        {
            _kernel.Bind<ILogger>().To<T>();
        }

        protected void RegisteredModule<T>() where T : IModule
        {
            _kernel.Bind<IModule>().To<T>();
        }

		protected CodeBlock As(IExecutionContext context, params IExecutionContext[] args)
		{
			return CreateCodeBlock(context, args);
		}

		protected CodeBlock Given(IExecutionContext context, params IExecutionContext[] args)
		{
			return CreateCodeBlock(context, args);
		}

		private CodeBlock CreateCodeBlock(IExecutionContext context, IEnumerable<IExecutionContext> args)
		{
			var codeBlock = new CodeBlock(_kernel);

			codeBlock.AddContext(context);

			if (args != null)
				codeBlock.AddContext(args);

			return codeBlock;
		}

		protected void Do<T>() where T : IExecutableStep
		{
			var step = _kernel.Get<T>();

			_pipeline.Run(step);
		}

		protected void Do<T>(Action<T> action) where T : IExecutableStep
		{
			var step = _kernel.Get<T>();

			action(step);

			_pipeline.Run(step);
		}

		protected IExecutionContext Get<T>() where T : IExecutionContext
		{
			return _kernel.Get<T>();
		}

		protected TResult Do<T, TResult>() where T : IExecutableStepWithReturnValue<TResult>
		{
			var step = _kernel.Get<T>();

			return _pipeline.RunAndReturnValue(step);
		}

		protected TResult Do<T, TResult>(Action<T> action) where T : IExecutableStepWithReturnValue<TResult>
		{
			var step = _kernel.Get<T>();

			action(step);

			return _pipeline.RunAndReturnValue(step);
		}
	}
}