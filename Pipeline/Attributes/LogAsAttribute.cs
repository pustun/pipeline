﻿using System;

namespace Pipeline.Attributes
{
	public class LogAsAttribute : Attribute
	{
		public string Text { get; private set; }

		public LogAsAttribute(string text)
		{
			Text = text;
		}
	}
}