﻿namespace Pipeline
{
	public interface IModule
	{
		void BeforeExecution(IStep step);

		void AfterExecution(IStep step);
	}
}