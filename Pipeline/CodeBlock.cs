﻿using System;
using System.Collections.Generic;
using Ninject;
using Pipeline;

namespace SeleniumProject.PipelineTests
{
	public class CodeBlock
	{
		private readonly IKernel _kernel;
		private List<IExecutionContext> _contextList = new List<IExecutionContext>();

		public void AddContext(IExecutionContext context)
		{
			context.AddContextBindings(_kernel);

			_contextList.Add(context);
		}

		public void AddContext(IEnumerable<IExecutionContext> context)
		{
			foreach (var executionContext in context)
			{
				executionContext.AddContextBindings(_kernel);
			}

			_contextList.AddRange(context);
		}

		public CodeBlock(IKernel kernel)
		{
			_kernel = kernel;
		}

		public void Execute(Action operations)
		{
			if (_contextList.Count == 0)
			{
				throw new ArgumentException("ContextList should be defined");
			}

			operations();

			UnbindContext();
		}

		private void UnbindContext()
		{
			foreach (var executionContext in _contextList)
			{
				executionContext.RemoveContextBindings(_kernel);
			}
		}
	}
}