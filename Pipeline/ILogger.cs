﻿using System;

namespace Pipeline
{
	public interface ILogger
	{
		void Log(string text);

		void Log(string format, params object[] args);

		void Log(Exception exception);
	}

	public class Logger : ILogger
	{
		public void Log(string text)
		{
			Console.WriteLine(text);
		}

		public void Log(string format, params object[] args)
		{
			Console.WriteLine(format, args);
		}

		public void Log(Exception exception)
		{
			Console.WriteLine(exception);
		}
	}
}