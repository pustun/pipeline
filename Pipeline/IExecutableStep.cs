﻿namespace Pipeline
{
	public interface IExecutableStep : IStep
	{
		void Execute();
	}
}